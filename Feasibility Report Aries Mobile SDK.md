# Aries Mobile SDK - Feasibility Report

# Table of Contents

* [Background](#background)
* [Key accomplishments](#key-accomplishments)
* [Deviations and takeaways](#deviations-and-takeaways)
* [Integrations and External Parties](#integrations-and-external-parties)
* [Interoperability](#interoperability)
* [Usability](#usability)
* [Deliverables](#deliverables)
  * [Specifications](#specifications)
  * [Source code](#source-code)
* [Publications](#publications)
* [Final words](#final-words)

# Background

As part of the eSSIF-Lab grant, Animo Solutions has created the Aries Mobile SDK. The main purpose of the Aries Mobile SDK is to make the development of mobile self-sovereign identity (SSI) solutions easier. The goal is to provide a simple, yet powerful API that is understandable for anyone, not just developers that are already in the self-sovereign identity space.

The SDK is built atop of Aries Framework JavaScript (AFJ). However, because a lot of the functionality that is required for creating mobile SSI solutions should be provided by the framework, extending the framework was a big part of the Aries Mobile SK project. On top of making the framework more suitable for mobile development, a set of mobile components that alleviate common practises in mobile SSI solutions was also created. 

Apart from creating these mobile components, we have taken AFJ to the next level by ensuring it is Aries Interoperability Profile 2.0 compliant and adding support for the latest standards. Additionally, a lot of work has been done on making working with the framework as developer friendly as possible. To achieve this, we have invested a lot in designing an API that keeps things as simple as possible and creating documentation that is clear and understandable.

# Goals versus achievements

The table below describes the status of each feature as mentioned in the proposal.

| Goal | Achievement against set goal |
|:---|----|
| Aries issue credential V2 protocol | We have added support for the [Aries issue credential v2 protocol](https://github.com/hyperledger/aries-rfcs/blob/main/features/0453-issue-credential-v2) in Aries Framework JavaScript. This protocol enables the framework to issue credentials in multiple formats. |
| Aries present proof v2 protocol | We have added support for the  [Aries present proof v2 protocol](https://github.com/hyperledger/aries-rfcs/tree/main/features/0454-present-proof-v2) in Aries Framework JavaScript. This protocol enables the framework to verify credentials of multiple formats. Part of this protocol is support for DIF's Presentation Exchange. For this purpose we are using the [PEX library](https://github.com/Sphereon-Opensource/pex) that Sphereon created as part of the eSSIF-Lab infrastructure 2 call. The integration with the PEX library is currently still in progress. |
| W3C/JSON-LD credential format | The framework is currently able to issue W3C/JSON-LD based credentials, which is part of the issue credential v2 work. After finishing the integration with the PEX library, the framework will also be able to verify this credential format. The estimated timeline for finishing the PEX library integration is 3 to 4 weeks. |
| BBS+ based credentials | We've added support for BBS+ signatures to the framework. This allows for  issuance, verification and proof derivation (used for selective disclosure) of BBS+ credentials. |
| Wallet import and export | We've added wallet import and export functionality to the framework, allowing users to import and export the contents of their wallet. This allows users to create backups of their wallet, as well as migrate the contents of their wallet between different Aries Framework JavaScript based agents. |
| Aries out-of-band protocol | We've implemented the [Aries out-of-band protocol](https://github.com/hyperledger/aries-rfcs/tree/main/features/0434-outofband) in the framework, allowing for issuance and verification of credentials without the need for a preexisting connection between the parties in question. |
| Aries DID exchange protocol | We've implemented the [Aries DID exchange](https://github.com/hyperledger/aries-rfcs/tree/main/features/0023-did-exchange) in the framework, enabling agents to exchange DIDs when establishing a DID based relationship. This protocol is mainly a prerequisite for out-of-band issuance and verification. |
| *Extensible DID resolver* | We’ve added a generic DID resolver that it easy to add support for additional DID methods over time. Additionally, we’ve implemented resolvers for `did:key` and `did:peer`.  |
| *The migration assistant* | With the addition of W3C based credentials, we changed internal storage data structures. To allow users to easily migrate between these old and new data structures, we’ve created a migration assistant. The assistant is extensible and can be used for future migrations as well. |
| Redux store | We have implemented the Redux store component as an extension of the framework. This component allows users of the framework that are creating mobile applications in React Native to easily integrate the agent state into their apps. |
| Mobile components | We are currently halfway through the implementation of the mobile components. These components enable users of the framework that are creating mobile applications in React Native to speed up common tasks such as biometric wallet unlock, the scanning of QR codes, handling of notifications and deep linking. The estimated timeline for finalising the mobile components is 2 to 3 weeks. |
| Documentation | We have started setting up and writing documentation for the framework and the mobile components. This work has started later than initially planned, because we found that the public APIs were still changing a lot. Now we have a more clear view on what the APIs will look like, we are better able to write documentation without having to continuously change it. The estimated timeline for the documentation is 3 to 4 weeks. |

# Deviations and takeaways

Although we haven’t deviated from the original goals as stated in the proposal and will likely finish the work soon, we have underestimated the speed of development in an open source project like Aries Framework JavaScript. As the global interest in Aries Framework JavaScript is growing, so is the community of different parties, each having their own unique set of interests. We have found that this introduces a challenge in the overall coordination of the project. As there were many parties with an interest in the work we were doing, we have tried to adapt our planning to fit their timelines as much as possible. Also, some of the work was taken up by community members with a shorter timeline on certain features. Although it initially felt like this would speed up the development and we could take on additional milestones, we found it also introduced a different set of challenges in guiding and supporting these community members. 

With multiple parties involved with varying levels of experience and expertise, we spent much more time on guidance, code reviews and working group discussions than expected. Additionally, this created a dependency for us on other developers in the community, making it harder to control the overall progression of the project. This assistance has however given us new insights in making the Aries Mobile SDK as starter-friendly as possible, as these community developers had different experience levels from our own. 

In order to make the deadline on every feature, we could have made compromises and rush the implementation of certain features. However, because one of our main goals is to make the framework as developer friendly as possible, we believe that it is very important to maintain a high standard in the quality of the framework. Poorly written code isn't going to help usability after all. Therefore we decided not to rush things and keep on reviewing the code of us and others thoroughly.

# Integrations and external parties

### Bloqzone

Bloqzone is a Dutch technology platform and infrastructure 3 participant that combines identification with a range of communication methods. Early on Bloqzone expressed their interest in using the Aries Mobile SDK to enable authorised calls in a session initiation protocol (SIP) based voice over internet protocol (VoIP) application. Since then, Animo has guided and helped integrating the Aries Mobile SDK into their software.

### Gimly

Gimly is a Dutch SSI company and infrastructure 2 participant that focuses on near field communication (NFC) technology. Gimly wants to use the Aries Mobile SDK for DIDComm messaging and other functionality that falls outside the scope of their NFC Bridge.

### Other parties

Various other parties outside of the eSSIF-Lab community will use the SDK for their proprietary solutions, Animo has several customers who will use it as well for their wallet projects. 

# Interoperability

The eSSIF-Lab grant has increased the interoperability of the framework in various ways. Firstly the addition of support for multiple credential formats allows for interoperability outside the Indy ecosystem. The added support for the W3C verifiable credential standard is a good example of this. In addition, the introduction of a generic and extensible DID resolver enables developers to easily add support for additional DID methods. Lastly, Aries Framework JavaScript integrates with the Aries Agent Test Harness (AATH). This is a testing suite that runs integration tests between different Aries implementations and therefore can be used to test for interoperability. We've extended these tests to test the work that was done as part of this grant.

# Usability

Aries Framework JavaScript is built with the mindset that SSI will only really take off if it can be applied by developers that are outside the 'SSI space'. This means that frameworks should not require developers to study the underlying theories for days, but should instead offer an intuitive API and clear documentation.

The work done as part of this grant required quite some refactoring of the pre-existing APIs, giving us the opportunity to rethink. We've taken a serious amount of effort to create an understandable API that abstracts away the unnecessary complexities, yet isn't opinionated and allows for advanced usage.

Although a clear API helps a lot, we understand that developers need well written documentation too. Therefore we strive to create clear documentation that goes beyond just explaining the API, but also includes tutorials for common practices like how to setup a DID based connection, how to issue a credential, etc. As mentioned previously in this document, we are still in the process of writing the documentation due to the frequent API changes during development. However, we expect to have a complete first draft in about 3 to 4 weeks.

# Deliverables

## Specifications

* [Functional specification](https://github.com/animo/aries-mobile-sdk/blob/main/functional-specification-of-aries-mobile-sdk-component.md)
* [Interface specification](https://github.com/animo/aries-mobile-sdk/blob/main/interface-specification-of-aries-mobile-sdk-component.md)
* [Envisioned interoperability](https://github.com/animo/aries-mobile-sdk/blob/main/envisioned-interoperability-with-others.md)
* Source code (see below)
* Feasibility report (this document)

## Source code

* [Aries Framework JavaScript](https://github.com/hyperledger/aries-framework-javascript)
  * Due to this project being an open source project, not all of the work that has been done is merged into the main branch yet. Some features are still under review, others have been scheduled to be merged at a later point. This is because releases of the framework are based on the main branch.
* Mobile components -- Some of the components are currently located in an Animo repository. Ownership will be transfered to Hyperledger at a later stage. 
  * [Redux Store](https://github.com/hyperledger/aries-framework-javascript-ext/tree/main/packages/redux-store)
  * [Keychain (biometric wallet lock)](https://github.com/animo/aries-mobile-components/tree/main/packages/keychain)
  * [QR Scanner](https://github.com/animo/aries-mobile-components/tree/main/packages/qr-scanner)
* Documentation (TBD)

# Publications
- [Blog](https://animo.id/project/animo-receives-eu-grant)

# Final words

We are very thankful to the eSSIF-Lab team for giving us the opportunity to do this project. We believe that the work that was made possible by the grant makes the development of mobile SSI applications in React Native a lot more accessible. We’d also like to thank the eSSIF-Lab team for all the support, frequent calls and connections. The Aries Mobile SDK, and Aries Framework JavaScript will be developed and maintained further by us (currently researching EBSI interop) and by the community to make sure any developer with JavaScript experience can implement SSI without having to become an expert. 


